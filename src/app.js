const chalk = require("chalk");
const rl = require("readline");

const input = rl.createInterface({
    input: process.stdin,
    output: process.stdout,
    terminal: false
});

input.setPrompt("Entrez la factorielle à calculer ((Q)(q)uitter) : ");
input.prompt();
input.on("line", answer => {
    if(answer.toLowerCase() === "q") process.exit();
    if(!/\d/.test(answer)) {
        console.log(chalk.red("seul les nombres sont autorisés !!"))
    } else {
        console.log(`La factorielle de ${ answer } = ` + chalk.yellow(factoriel(answer)));
    }
    input.prompt();
});

function factoriel(val) {
    if ( val - 1 !== 0 ) {
        return val * factoriel(val - 1);
    }
    return val;
}

